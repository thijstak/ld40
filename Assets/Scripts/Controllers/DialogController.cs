﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Controllers
{
	public class DialogController : MonoBehaviour
	{
		public Text Message = null;
		public Text Speaker = null;
		public Image Portrait = null;

		public bool IsOpen { get { return gameObject.activeSelf; } }

		public void Start()
		{
			GameCore.Instance.DialogController = this;
			gameObject.SetActive(false);
		}

		public void ShowDialog(Sprite portrait, string speaker, string message)
		{
			gameObject.SetActive(true);
			Portrait.sprite = portrait;
			Speaker.text = speaker;
			Message.text = message;
		}

		public void Close()
		{
			gameObject.SetActive(false);
		}
	}
}
