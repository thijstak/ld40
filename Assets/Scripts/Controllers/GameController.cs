﻿using Assets.Scripts.Map;
using Assets.Scripts.World.Genrators;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
	public class GameController : MonoBehaviour
	{
		public int WorldSizeToUse = 10;

		private MapManager mapManager;

		private GameCore core;

		void Awake()
		{
			GameCore.CreateInstance();
		}

		public void Start()
		{
			core = GameCore.Instance;

			core.World = new World.World();
			core.World.InitializeWorld(WorldSizeToUse);
			core.World.GenerateWorld(new PureRandomWorldGenerator());

			mapManager = GetComponent<MapManager>();
			mapManager.Initialize();

			core.DialogController.ShowDialog(null, "Test Message", "Click here.");
		}
	}
}
