﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	public float MovementSpeed = 1.0f;
	public float ScrollSpeed = 1.0f;

	public int CameraMin = 3;
	public int CameraMax = 25;

	// Use this for initialization
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{
		int worldMin = 0;
		int worldMax = GameCore.Instance.World.WorldSize;

		float xDelta = Input.GetAxis("Horizontal") * MovementSpeed;
		float yDelta = Input.GetAxis("Vertical") * MovementSpeed;
		Vector3 movementVector = new Vector3(xDelta, yDelta, 0) * Time.deltaTime * (Camera.main.orthographicSize / 4);

		gameObject.transform.position += movementVector;

		float x = gameObject.transform.position.x;
		float y = gameObject.transform.position.y;

		x = Mathf.Clamp(x, worldMin, worldMax);
		y = Mathf.Clamp(y, worldMin, worldMax);

		gameObject.transform.position = new Vector3(x, y, -10);

		float scrollDelta = Input.GetAxis("Mouse ScrollWheel");
		Camera.main.orthographicSize -= ScrollSpeed * scrollDelta * Time.deltaTime * Camera.main.orthographicSize;
		Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, CameraMin, CameraMax);
	}
}
