﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseController : MonoBehaviour
{
	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		if (GameCore.Instance.InPauseMode) return;

		if (Input.GetMouseButton(0))
		{
			if (EventSystem.current.IsPointerOverGameObject())
			{
				return;
			}

			// Need to do state management here.
			OnClick();
		}
	}

	private void OnClick()
	{
		// Get the mouse position.
		Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		// Get the tile id.
		int x = (int)mousePosition.x;
		int y = (int)mousePosition.y;

		Debug.Log(x + ", " + y);
	}
}
