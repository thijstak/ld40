﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Helpers;
using UnityEngine;

public class EndGameController : MonoBehaviour
{

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}

	public void ReturnToMenuClick()
	{
		SceneHelpers.LoadScene(SceneHelpers.SceneNames.StartMenu);
	}
}
