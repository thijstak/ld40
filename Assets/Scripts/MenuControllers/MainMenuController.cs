﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Helpers;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}

	public void StartGameClick()
	{
		SceneHelpers.LoadScene(SceneHelpers.SceneNames.MainGame);
	}
}
