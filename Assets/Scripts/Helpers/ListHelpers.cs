﻿using System.Collections.Generic;
using System.Linq;


public static class ListHelpers
{
	public static T Random<T>(this IEnumerable<T> source)
	{
		int number = UnityEngine.Random.Range(0, source.Count());
		return source.ElementAt(number);
	}
}
