﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Helpers
{
	public static class SceneHelpers
	{
		public enum SceneNames
		{
			StartMenu = 0,
			MainGame = 1,
			EndGame = 2
		}

		/// <summary>
		/// Loads the scene.
		/// </summary>
		/// <param name="name">The name of the scene to load.</param>
		public static void LoadScene(SceneNames name)
		{
			// Load the requested scene.
			SceneManager.LoadScene((int)name);
		}
	}
}
