﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework.Constraints;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Map
{
	public class MapManager : MonoBehaviour
	{
		public Sprite[] GrassTiles;
		public Sprite[] ForestTiles;
		public Sprite[] WaterTiles;
		public Sprite[] MountainTiles;

		public Tilemap BoardMap;
		public Tilemap IconMap;

		private Dictionary<GroundTypes, List<Tile>> tiles;

		public void Initialize()
		{
			GenerateTiles();
			BuildMap();
		}

		/// <summary>
		/// Builds the map.
		/// </summary>
		public void BuildMap()
		{
			Debug.Log(GameCore.Instance.World.WorldSize);
			for (int y = 0; y < GameCore.Instance.World.WorldSize; y++)
			{
				for (int x = 0; x < GameCore.Instance.World.WorldSize; x++)
				{
					BoardMap.SetTile(new Vector3Int(x, y, 0), tiles[GameCore.Instance.World.GetTile(x, y)].Random());
				}
			}
		}

		void GenerateTiles()
		{
			// Ignore existing tiles for now...
			tiles = new Dictionary<GroundTypes, List<Tile>>();

			foreach (object value in Enum.GetValues(typeof(GroundTypes)))
			{
				tiles.Add((GroundTypes)value, new List<Tile>());
			}

			foreach (Sprite tile in GrassTiles)
			{
				tiles[GroundTypes.Grass].Add(CreateTile(tile));
			}

			foreach (Sprite tile in ForestTiles)
			{
				tiles[GroundTypes.Forest].Add(CreateTile(tile));
			}

			foreach (Sprite tile in WaterTiles)
			{
				tiles[GroundTypes.Water].Add(CreateTile(tile));
			}

			foreach (Sprite tile in MountainTiles)
			{
				tiles[GroundTypes.Mountain].Add(CreateTile(tile));
			}
		}

		/// <summary>
		/// Creates a new tile.
		/// </summary>
		/// <param name="sprite">The sprite.</param>
		/// <returns></returns>
		Tile CreateTile(Sprite sprite)
		{
			Tile tile = new Tile();
			tile.sprite = sprite;
			tile.colliderType = Tile.ColliderType.None;
			tile.color = Color.white;
			return tile;
		}
	}
}
