﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Text;
using Assets.Scripts.Controllers;

namespace Assets.Scripts
{
	public class GameCore
	{
		public static GameCore Instance;
		public DialogController DialogController { get; set; }

		public bool InPauseMode { get; set; }

		public static void CreateInstance()
		{
			if (Instance == null)
			{
				Instance = new GameCore();
			}
		}

		public World.World World { get; set; }
	}
}
