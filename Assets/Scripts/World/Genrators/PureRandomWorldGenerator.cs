﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.World.Genrators
{
	public class PureRandomWorldGenerator : IWorldGenerator
	{
		#region Implementation of IWorldGenerator

		private readonly List<GroundTypes> tiles = Enum.GetValues(typeof(GroundTypes))
			.OfType<GroundTypes>().ToList();

		public void GenerateMap(World world)
		{
			for (int y = 0; y < world.WorldSize; y++)
			{
				for (int x = 0; x < world.WorldSize; x++)
				{
					world.SetTile(x, y, tiles.Random());
				}
			}
		}

		#endregion
	}
}