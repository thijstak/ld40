﻿using System.Text;

namespace Assets.Scripts.World
{
	public class World
	{
		public World()
		{

		}

		/// <summary>
		/// This contains the ground floors of the world map.
		/// </summary>
		private GroundTypes[,] worldMap;

		public int WorldSize { get; protected set; }

		public void InitializeWorld(int size)
		{
			WorldSize = size;
			worldMap = new GroundTypes[size, size];
		}

		public void GenerateWorld(IWorldGenerator generator)
		{
			generator.GenerateMap(this);
		}

		public void SetTile(int x, int y, GroundTypes type)
		{
			worldMap[x, y] = type;
		}

		public GroundTypes GetTile(int x, int y)
		{
			return worldMap[x, y];
		}
	}
}
