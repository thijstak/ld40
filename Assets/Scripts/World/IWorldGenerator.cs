﻿namespace Assets.Scripts.World
{
	public interface IWorldGenerator
	{
		void GenerateMap(World world);
	}
}